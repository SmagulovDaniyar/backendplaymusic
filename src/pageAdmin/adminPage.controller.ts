import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreateUsersDto } from '../user/dto/create-users.dto';
import { UserService } from '../user/services/user.service';
import { JwtAuthGuard } from '../user/guard/jwt.guard';
import { UserEntity } from '../user/entities/user.entity';

@Controller('page')
export class AdminPageController {
  constructor(private readonly userService: UserService) {}
  @UseGuards(JwtAuthGuard)
  @Delete('page/:id')
  async deleteUser(@Param('id') id: number) {
    return this.userService.delete(id);
  }

  //@UseGuards(JwtAuthGuard)
  @Get()
  async getRegisUser(): Promise<UserEntity[]> {
    const regisUsers = this.userService.getRegisUsers();
    return regisUsers;
  }
}
