import { Field, ID, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';

@InputType()
export class AuthDto {
  @IsString()
  email: string;

  @IsString()
  password: string;
}
