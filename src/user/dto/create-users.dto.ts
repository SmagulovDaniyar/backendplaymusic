import { IsString } from 'class-validator';

export class CreateUsersDto {
  id: number;
  @IsString()
  email: string;
}
