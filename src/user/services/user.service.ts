import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/user.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateUsersDto } from '../dto/create-users.dto';
import { UpdateUsersDto } from '../dto/update-users.dto';
import { compare, compareSync, genSaltSync, hashSync } from 'bcryptjs';
import { AuthDto } from '../dto/auth.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly jwtService: JwtService,
  ) {}

  async updateUser(dto: UpdateUsersDto): Promise<UpdateResult> {
    //обновляет юзера уже с данными
    const salt = genSaltSync(10);

    const newUser: UserEntity = new UserEntity();
    //newUser.id=dto.id
    newUser.password = hashSync(dto.password, salt);
    newUser.isRegis = true;

    return this.userRepository.update({ email: dto.email }, { ...newUser });
  }

  async createAdmin(dto: AuthDto) {
    const salt = genSaltSync(10);

    const newUser: UserEntity = new UserEntity();
    newUser.email = dto.email;
    newUser.password = hashSync(dto.password, salt);
    return this.userRepository.save(newUser);
  }

  async saveUser(dto: CreateUsersDto): Promise<UserEntity> {
    //Сохраняем юзера у которого только приходить е-майл
    return this.userRepository.save(dto);
  }
  async findUserByEmail(email: string) {
    //Искать по почте
    return this.userRepository.findOne({ email });
  }

  async getOneUser(id: number): Promise<UserEntity> {
    return await this.userRepository.findOne({ id });
  }

  async getRegisUsers(): Promise<UserEntity[]> {
    //для получение юзеров которые прошли регистрацию
    return await this.userRepository.find({ isRegis: true });
  }

  async delete(id: number): Promise<number> {
    // удаления пользовотеля
    await this.userRepository.delete({ id });
    return id;
  }

  async validate(
    email: string,
    password: string,
  ): Promise<Pick<UserEntity, 'email'>> {
    const user = await this.findUserByEmail(email);

    if (!user) {
      throw new UnauthorizedException('Не был найден е-майл');
    }
    const isCorrectPass = compareSync(password, user.password);

    if (!isCorrectPass) {
      throw new UnauthorizedException('Не был найден парол');
    }
    return { email: user.email };
  }

  async login(email: string) {
    const payload = { email };

    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
