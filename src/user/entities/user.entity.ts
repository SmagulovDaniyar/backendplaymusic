import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@Entity('user_third')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  email: string;
  @Column({ default: ' ' })
  password: string;
  @CreateDateColumn()
  createdAt: Date;
  @Column({ default: false })
  isRegis: boolean;
  @UpdateDateColumn()
  lastAuth: Date;
}
